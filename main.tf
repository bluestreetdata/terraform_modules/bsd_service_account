module "bsd_service_account" {
  source            = "./modules/core"
  sa_name           = var.sa_name
  project_id        = var.project_id
  company_prefix    = var.company_prefix
  extended_sa_roles = var.extended_sa_roles
  extra_project_id  = var.extra_project_id
}
