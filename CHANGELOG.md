# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.2](https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account/-/tree/v0.2.2) - 2022-08-25

### Updated
- Developer tools to version 1
## [0.2.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account/-/tree/v0.2.1) - 2022-03-30
### Fixed
- Cloud Run now also can have extra project

## [0.2.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account/-/tree/v0.2.0) - 2022-03-30
### Added
- Extra project list variable

## [0.1.0](https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account/-/tree/v0.1.0) - 2022-01-16
### Added
- Cloud run service account
### Changed
- Modular structure now applied
## [0.0.2](https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account/-/tree/v0.0.2) - 2021-05-30
### Added
- Adds big query basic permissions
### Changed
- Updates tests to use for loops
## [0.0.1](https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account/-/tree/v0.0.1) - 2021-05-29
### Added
- Initial release


