variable "sa_name" {
  description = "The name of the service account - Note it must match ^[a-z](?:[-a-z0-9]{4,22}[a-z0-9])"
}

variable "project_id" {
  description = "The project in which the service account will be created"
}

variable "company_prefix" {
  type        = string
  description = "the unique prefix for resource naming"
}

variable "extended_sa_roles" {
  type        = list(any)
  description = "A list of Roles that are to be applied as a google_project_iam_member"
  default     = []

}

variable "extra_project_id" {
  type        = list(string)
  description = "A list of project_id in which to also apply the permissions"
  default     = []

}
