/**
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */



output "name" {
  description = "The name of the Service Account."
  value       = module.service_account.name
}


output "email" {
  description = "The email of the service account"
  value       = module.service_account.email
}
output "iam_email" {
  description = "The IAM formatted email address"
  value       = module.service_account.iam_email

}

output "extra_project_id" {
  value       = module.service_account.extra_project_id
  description = "The list if extra projectids who also have the permissions"

}
