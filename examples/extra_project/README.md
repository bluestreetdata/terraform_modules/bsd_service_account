# Standard Example

This example illustrates how to use the `bsd_service_account` module.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| extended\_sa\_roles | n/a | `list(any)` | `[]` | no |
| extra\_project\_id | A list of project\_id in which to also apply the permissions | `list(any)` | `[]` | no |
| project\_id | The project in which the buckets will be created | `any` | n/a | yes |
| sa\_name | The name of the service account | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| email | The email of the service account |
| extra\_project\_ids | The list if extra projectids who also have the permissions |
| iam\_email | The IAM formatted email address |
| name | The name of the Service Account. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

To provision this example, run the following from within this directory:
- `terraform init` to get the plugins
- `terraform plan` to see the infrastructure plan
- `terraform apply` to apply the infrastructure build
- `terraform destroy` to destroy the built infrastructure
