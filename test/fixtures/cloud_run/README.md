


<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| project\_id | The project in which the service account will be created | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| email | The Email of the service account |
| iam\_email | The IAM formated email |
| name | The name of the Service Account. |
| project\_id | The ID of the project in which resources are provisioned. |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->