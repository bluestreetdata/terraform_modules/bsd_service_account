# copyright: 2018, The Authors
title 'Verifying the service account has applied in the other projects'
IAM_EMAIL = attribute('iam_email', description:'The output email of the service account')
EXTRA_PROJECT_ID = attribute('extra_project_id', description:'The list of extra project ids to check')
control "extra-project-permission" do
  ['roles/storage.objectViewer','roles/bigquery.jobUser','roles/bigquery.metadataViewer'].each do |role|
    EXTRA_PROJECT_ID.each do |project_id| 
      describe google_project_iam_binding(project: project_id, role:role) do
        it { should exist }
        its('members') {should include IAM_EMAIL}
      end
    end
  end
end
