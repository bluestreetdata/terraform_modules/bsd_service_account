# copyright: 2018, The Authors
title 'Verifying the service account has been set up correctlty'
NAME = attribute('name', description: 'the name of the created service account')
EMAIL = attribute('email', description:'The output email of the service account')
IAM_EMAIL = attribute('iam_email', description:'The output email of the service account')
control "account-exists" do
  title 'Check the account has been made'
  describe google_service_account(project: attribute("project_id"), name: EMAIL) do
    it { should exist }
  end
end

control "basic-permissions" do
  ['roles/storage.objectViewer','roles/bigquery.jobUser','roles/bigquery.metadataViewer'].each do |role|
    describe google_project_iam_binding(project: attribute("project_id"), role:role) do
      it { should exist }
      its('members') {should include IAM_EMAIL}
    end
  end
end

control "excluded-permissions" do
  ['roles/owner','roles/editor'].each do |role|
    describe google_project_iam_binding(project: attribute("project_id"), role:role) do
      its('members') {should_not include IAM_EMAIL}
    end
  end
end