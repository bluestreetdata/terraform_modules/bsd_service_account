# copyright: 2018, The Authors
title 'Verifying the cloud run  service account has been set up correctlty'
NAME = attribute('name', description: 'the name of the created service account')
EMAIL = attribute('email', description:'The output email of the service account')
IAM_EMAIL = attribute('iam_email', description:'The output email of the service account')

control "run-permissions" do
  ['roles/run.invoker','roles/pubsub.publisher','roles/pubsub.subscriber'].each do |role|
    describe google_project_iam_binding(project: attribute("project_id"), role:role) do
      it { should exist }
      its('members') {should include IAM_EMAIL}
    end
  end
end
