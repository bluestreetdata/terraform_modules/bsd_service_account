<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| project\_id | the name of the project - premade to deploy in | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| project\_id | n/a |
| sa\_key | n/a |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->