output "email" {
  value       = module.bsd_service_account.email
  description = "The email address of the created service account"
}
output "iam_email" {
  value       = module.bsd_service_account.iam_email
  description = "Input to IAM read value for service account email eg 'serviceAccount:sa_email@your_project.iam.google.com'"
}

output "name" {
  value       = module.bsd_service_account.email
  description = "The created name of the service account will be 'company_prefix-sa_name'"
}

output "extra_project_id" {
  value       = var.extra_project_id
  description = "A list of project_id in which to also apply the permissions"

}
