resource "google_project_iam_member" "project_permission" {
  for_each = toset(var.permissions)
  project  = var.project_id
  role     = each.value
  member   = "serviceAccount:${var.service_account_email}"
}
