# Cloud Run Service Account



## Usage

Basic usage of this module is as follows:

```hcl
module "bsd_cloud_run_service_account" {
  source         = "git::https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account.git//modules/cloud_run?ref=tags/v0.1.0"
  project_id     = "<PROJECT ID>"
  company_prefix = "<COMPANY PREFIX>"
  sa_name        = "name to describe here"
}
```

Functional examples are included in the
[examples](./examples/) directory.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| extended\_sa\_roles | A list of Roles that are to be applied as a google\_project\_iam\_member | `list(any)` | `[]` | no |
| project\_id | The project in which the service account will be created | `any` | n/a | yes |
| sa\_name | The name of the service account - Note it must match ^[a-z](?:[-a-z0-9]{4,22}[a-z0-9]) | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| email | The email address of the created service account |
| iam\_email | Input to IAM read value for service account email eg 'serviceAccount:sa\_email@your\_project.iam.google.com' |
| name | The created name of the service account will be 'company-prefix=sa\_name' |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

