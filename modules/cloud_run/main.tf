locals {
  cr_basic_roles = [
    "roles/run.invoker",
    "roles/pubsub.publisher",
    "roles/pubsub.subscriber"
  ]

}
module "service_account" {
  source            = "../core"
  project_id        = var.project_id
  company_prefix    = var.company_prefix
  sa_name           = var.sa_name
  extended_sa_roles = concat(local.cr_basic_roles, var.extended_sa_roles)
  extra_project_id  = var.extra_project_id
}
