locals {
  service_account_name = "${var.company_prefix}-${var.sa_name}"
  basic_sa_roles = [
    "roles/storage.objectViewer",
    "roles/bigquery.metadataViewer",
    "roles/bigquery.jobUser"
  ]
}


resource "google_service_account" "service_account" {
  account_id   = local.service_account_name
  display_name = local.service_account_name
  project      = var.project_id
}

resource "time_sleep" "wait_5_seconds" {
  depends_on      = [google_service_account.service_account]
  create_duration = "5s"
}
# https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_service_account
# service accounts are eventually consistend - this will help with creatation before permissions
module "basic_permission" {
  depends_on = [
    time_sleep.wait_5_seconds
  ]
  source                = "../project_permissions"
  project_id            = var.project_id
  permissions           = local.basic_sa_roles
  service_account_email = google_service_account.service_account.email
}

module "extended_permission" {
  depends_on = [
    time_sleep.wait_5_seconds
  ]
  source                = "../project_permissions"
  project_id            = var.project_id
  permissions           = var.extended_sa_roles
  service_account_email = google_service_account.service_account.email
}


module "basic_permission_extra_projects" {
  depends_on = [
    time_sleep.wait_5_seconds
  ]
  for_each              = toset(var.extra_project_id)
  source                = "../project_permissions"
  project_id            = each.value
  permissions           = local.basic_sa_roles
  service_account_email = google_service_account.service_account.email
}

module "extended_permission_extra_projects" {
  depends_on = [
    time_sleep.wait_5_seconds
  ]
  for_each              = toset(var.extra_project_id)
  source                = "../project_permissions"
  project_id            = each.value
  permissions           = var.extended_sa_roles
  service_account_email = google_service_account.service_account.email
}
