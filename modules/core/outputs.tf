output "email" {
  value       = google_service_account.service_account.email
  description = "The email address of the created service account"
}
output "iam_email" {
  value       = "serviceAccount:${google_service_account.service_account.email}"
  description = "Input to IAM read value for service account email eg 'serviceAccount:sa_email@your_project.iam.google.com'"

}
output "name" {
  value       = google_service_account.service_account.name
  description = "The created name of the service account will be 'company-prefix=sa_name'"
}

output "extra_project_id" {
  value       = var.extra_project_id
  description = "The list of extra project ids that had also had permissions applied"

}
