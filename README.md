# bsd-service_account

This module was generated from [bsd-module-template](https://gitlab.com/bluestreetdata/terraform_modules/bsd_module_tempate/), which by default generates a module that simply creates a GCS bucket. As the module develops, this README should be updated.

The resources/services/activations/deletions that this module will create/trigger are:

- A service account in the project suppplied

## Usage

Basic usage of this module is as follows:

```hcl
module "bsd_service_account" {
  source         = ""git::https://gitlab.com/bluestreetdata/terraform_modules/bsd_service_account.git//?ref=tags/v0.1.0""
  project_id     = "<PROJECT ID>"
  company_prefix = "<COMPANY PREFIX>"
  sa_name        = "name to describe here"
}
```

Functional examples are included in the
[examples](./examples/) directory.

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| company\_prefix | the unique prefix for resource naming | `string` | n/a | yes |
| extended\_sa\_roles | A list of Roles that are to be applied as a google\_project\_iam\_member | `list(any)` | `[]` | no |
| extra\_project\_id | A list of project\_id in which to also apply the permissions | `list(any)` | `[]` | no |
| project\_id | The project in which the service account will be created | `any` | n/a | yes |
| sa\_name | The name of the service account - Note it must match ^[a-z](?:[-a-z0-9]{4,22}[a-z0-9]) | `any` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| email | The email address of the created service account |
| iam\_email | Input to IAM read value for service account email eg 'serviceAccount:sa\_email@your\_project.iam.google.com' |
| name | The created name of the service account will be 'company\_prefix-sa\_name' |

<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->

## Requirements

These sections describe requirements for using this module.

### Software

The following dependencies must be available:

- [Terraform][terraform] v0.12
- [Terraform Provider for GCP][terraform-provider-gcp] plugin v2.0

### Service Account

A service account with the following roles must be used to provision
the resources of this module:

- Storage Admin: `roles/storage.admin`

The [Project Factory module][project-factory-module] and the
[IAM module][iam-module] may be used in combination to provision a
service account with the necessary roles applied.

### APIs

A project with the following APIs enabled must be used to host the
resources of this module:

- Google Cloud Storage JSON API: `storage-api.googleapis.com`

The [Project Factory module][project-factory-module] can be used to
provision a project with the necessary APIs enabled.

## Contributing

Refer to the [contribution guidelines](./CONTRIBUTING.md) for
information on contributing to this module.

[iam-module]: https://registry.terraform.io/modules/terraform-google-modules/iam/google
[project-factory-module]: https://registry.terraform.io/modules/terraform-google-modules/project-factory/google
[terraform-provider-gcp]: https://www.terraform.io/docs/providers/google/index.html
[terraform]: https://www.terraform.io/downloads.html
